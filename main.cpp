
#include "MyStack.h"

void multipliers(int value, MyStack<int>);

int main() {
    int value;
    MyStack<int> stack1, stack2;
    cout << "Введите число: ";
    cin >> value;
    multipliers(value, stack1);
    cout << value << " = ";
    while (!stack1.empty()) {
        stack2.push(*(stack1.top_inf()));
        cout << *stack1.top_inf() << ' ';
        stack1.pop();
        cout << (!stack1.empty() ? " * " : "\n");
    }
    cout << value << " = ";
    while (!stack2.empty()) {
        cout << *stack2.top_inf() << ' ';
        stack2.pop();
        cout << (!stack2.empty() ? " * " : "\n");
    }
    return 0;
}


void multipliers(int value, MyStack<int> stack) {
    int divider = 2;
    while ((value > 1) && (divider <= value)) {
        if (value % divider == 0) {
            stack.push(divider);
            value = value / divider;
            divider = 2;
        } else {
            ++divider;
        }
    }
}