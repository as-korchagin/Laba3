#ifndef MyStack_h
#define MyStack_h

#include <iostream>

using namespace std;

template<class INF, class FRIEND>
class ListNode {
private:
    INF value;
    ListNode *prev;

    ListNode() { prev = nullptr; }

    friend FRIEND;
};

template<class INF>
class MyStack {
private:
    typedef class ListNode<INF, MyStack<INF> > Node;
    Node *top;
public:
    MyStack() {
        top = new Node();
    };

    ~MyStack() {
//        delete top;
    };

    bool empty() {
        return top->prev == nullptr;
    };

    bool push(INF elem) {
        Node *node = new Node();
        node->prev = top->prev;
        node->value = top->value;
        top->prev = node;
        top->value = elem;
        return true;
    };

    bool pop() {
        if (top->prev != nullptr) {
            Node *del = top;
            top = top->prev;
            delete del;
            return true;
        } else return false;
    };

    INF *top_inf() {
        return &(top->value);
    };
};


#endif
